#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

import cgitb
import datetime
import os


cgitb.enable()

print("Content-Type: text/html")
print()
print(f"""
<!DOCTYPE html>
<html>
  <head>
    <title>Hello, world, from python!</title>
  </head>
  <body>
    <p>Hello, world from python!</p>
    <p>Currrent date (in ISO formnat): {datetime.datetime.now().isoformat()}<p/>
    <p>Your browser: {os.environ.get('HTTP_USER_AGENT', '')}<p/>
  </body>
</html>
""")



