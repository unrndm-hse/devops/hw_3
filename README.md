HomeWork 3
==========

info
----
ports:
* 8080, redirects to 8443 with https
* 8443
* 8081

command to generate cet:
```terminal
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout cert.key -out cert.crt
```
